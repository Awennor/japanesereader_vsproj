﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pidroh.JapaneseTextReader.analytics.Tests
{
    [TestClass()]
    public class LearnerProfileTests
    {
        [TestMethod()]
        public void IsKnownWordTest()
        {
            var prof = new LearnerWordDataAccessor();
            const string Word = "bla";

            Assert.IsFalse(prof.IsKnownWord(Word));
            prof.LearnNewWord(Word);
            Assert.IsTrue(prof.IsKnownWord(Word));
        }
    }
}