﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JapaneseTextReader;

namespace pidroh.JapaneseTextReader
{
    class TextMetaDataHolder
    {

        public List<TextMetaData> Texts { get; } = new List<TextMetaData>();

        internal void Add(TextMetaData meta)
        {
            Texts.Add(meta);
        }
    }
}
