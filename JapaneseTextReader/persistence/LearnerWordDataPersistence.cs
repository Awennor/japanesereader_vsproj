﻿using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader.persistence
{
    [Serializable]
    public class LearnerWordDataPersistence
    {
        public LearnerWordData_ExerciseTypeData MeaningData { get; set; } = new LearnerWordData_ExerciseTypeData();
        public LearnerWordData_ExerciseTypeData ReadingData { get; set; } = new LearnerWordData_ExerciseTypeData();
        public void ClearAllData()
        {
            MeaningData.ClearAllData();
            ReadingData.ClearAllData();
        }

    }

    [Serializable]
    public class LearnerWordData_ExerciseTypeData
    {
        internal Dictionary<string, WordAnalyticsData> WordDatas { get; set; } = new Dictionary<string, WordAnalyticsData>();
        internal List<string> IgnoreWords { get; set; } = new List<string>();
        internal Dictionary<string, WordMiscData> WordMiscDatas { get; set; } = new Dictionary<string, WordMiscData>();

        internal void ClearAllData()
        {
            WordDatas.Clear();
            IgnoreWords.Clear();
            WordMiscDatas.Clear();
        }
    }
}
