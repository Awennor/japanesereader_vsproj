﻿using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader.persistence
{

    
    [Serializable]
    public class LearnerData
    {
        internal LearnerTextProgress LearnerTextProgress { set; get; } = new LearnerTextProgress();
        public LearnerWordDataPersistence learnerWordData { get; set; } = new LearnerWordDataPersistence();

    }
}
