﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader.persistence
{
    [Serializable]
    internal class LearnerTextProgress
    {
        private Dictionary<string, TextProgress> Progress { set; get; } = new Dictionary<string, TextProgress>();

        internal TextProgress GetProgress(string title)
        {
            if (!Progress.ContainsKey(title))
            {
                Progress[title] = new TextProgress();
            }
            return Progress[title];
        }
    }

    [Serializable]
    public class TextProgress
    {
        List<int> Progress { set; get; } = new List<int>();
        internal Dictionary<int, int> GoalProgress { get; set; } = new Dictionary<int, int>();

        internal void SetProgress(int v, int value)
        {
            while (Progress.Count <= v) {
                Progress.Add(0);
            }
            Progress[v] = value;
        }

        internal int GetProgress(int v)
        {
            while (Progress.Count <= v)
            {
                Progress.Add(0);
            }
            return Progress[v];
        }
    }
}
