﻿using JapaneseTextReader;
using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader.persistence
{
    public static class PersistenceExtension
    {



        public static void Save(this JapaneseReaderSuper jrs, Action<string, object> saveMethod, string prefix)
        {

            string newPrefix = prefix + "_jrs";

            //jrs.Learner.Save(saveMethod, newPrefix);
            //jrs.Jr.Save(saveMethod, newPrefix);
        }

        public static void Save(this LearnerWordDataAccessor l, Action<string, object> saveMethod, string prefix)
        {
            string newPrefix = prefix + "_learnerprofile";
            saveMethod(newPrefix + "_worddatas", l.WordDatas);
            saveMethod(newPrefix + "_ignorewords", l.IgnoreWords);
            saveMethod(newPrefix + "_interactionnumber", l.InteractionNumber);
        }
        public static void Save(this JapaneseReader jr, Action<string, object> saveMethod, string prefix)
        {
            string newPrefix = prefix + "_jr";
            saveMethod(newPrefix + "_currentseg", jr.Advancer.CurrentSegment);
            saveMethod(newPrefix + "_currentword", jr.Advancer.CurrentWord);
        }
    }
}
