﻿phrase navigation

change scheduling system to time-based
commit

*change unitysuper reader to
 -do all reading exercises
 -check for unknown words
 -review unknown words
 -review reading exercises
 -advance phrase
 -repeat
*show phrase with bolded word
*trim words
*convert reading hiragana
*port to unity
*do not exercise reading of words already known
*do not exercise words which have obvious reading
*make sure exercise has a phrase
*Make every exercise type have it's misc data too D: so each one can have it's associated phrases separately
*create logic to make reading exercise be saved IF exercise is correct
*Fix errors that popped up
*readingexercisemanagement shuold then talk to the analytics data accessor thing
*LearnerWordData => separated into accessor and persistence classes
*created a class to handle readingexercise management
*Create new code (new static method called from main) for doing the flow of writing exercises learning
*created new structure for storing found reading
*succesful test of reading exercise
*decided to use partchoiceexercise as reading exercise
*add reading of phrase to textmetadata
*fixed bug in superreaderunity
*create button to wipe out word data
*make phrase shown during exercise unity
*create space in exercise UI for phrase
*associate phrase with word when learning word
*try out reverse problem
*fix return button problem
*show progress at text selection
*make new words take longer to be exercised
*don't reload app if already loaded
*create scene to create textmetadata assets
*scroll down
*automatically show empty strings
*show previous phrases when opening scene
*code to get tactics ogre scripts
*button to return to text selection
*meeting doc
*mac build
*stop too much advancing
*create wipe progress button
*Make reader scene start the picked text
*add genericdata thing to the unity project for inter-scene communication
*Create scene to pick text
*create class that will hold a library of textmetadata and title
*save progress
*make word review choice buttons bigger... :(
*port to unity
*create readerview prefab
*create word selector prefab
*write new interface mockups
*Try it out with different text on console
*Add textmetadata words if learned words are not enough
*Create Full.6 reading on console
*automatically review while reading
*At each phrase add choice to add unknown words
*increase text size
*change new text color
*Keep adding phrases cumulatively while reading
*ignore list support
*try out always showing the phrase
*test
*add button for automated learning (automatically choose from new word, review or phrase)
*retain punctuation during segmentation
*Phrase learning on the prototype
*Create menu-based japanese learning on console (prototype)
*create methods in navigator according to the menu necessities
*add readings to the dictionary maker as desambiguation
*take a look at some of the words removed
*make words not repeat
*create review method
*show exercise result on console when exercise is done
*show button to press when showing exercise
*adapt exerciseview on console for custom show question method
*create japanese reader navigator 
*setup mecab on console app
*get the method to open up the text file in the repository
*Create a window that can show japanese text but does not need confirmation
*make japanese exercises more likely to separate into 3 parts
*Think up how to implement exercise difficulty
*add difficulty transition to learnerprofile
*add shown parts based on difficulty
*add difficulty argument to exercise generator (optional)
*create support in partchooseexercise for initially shown parts
*port word list exercises to unity in a new scene
*change scheduling to a system similar to anki
*create class to do exercises from a list of words
*create review strength formula
*add reviewing
*create new generator that generates from a pool of dummy words (?)
*getwordtoreview method
*start reviewing at a certain threeshold method
*stop learning repeated words
*keep track of cumulative correctness number
*keep track of last review interaction number
*keep track of known words
*hide problem UI after solving problem
*use introduce word UI
*create introduce word UI
*make result widget disappear on show press and on show exercise
*integrate with actual exercises
*create callback from view to controller for end show result
*create button to end show result
*Show result widget depending on result
*Create result widget
*make exercise go through every part of a word
*create callback for button on the view
*create view for exercise on unity
*show some stuff on unity debug
*create unity project
*remove repeats
*make exercises keep advancing regardless of correctness
*introduce word
*write full basic software flow for first version of the software
*investigate why dollar isn't being included (substantive>suffix)
*ignore numbers on parser
*Make the main code feed the controller with the model data
*Create controller for partexerciseview
*Use the general text information to create a method that generates an exercise for one segment
*generate text information containing parts and valid words (parsed words with meanings)
*Define exercise class



--Full Reading
----Text advances by itself, phrase by phrase, with audio playing
----can pause at anytime to add words, word selection UI pops up and you can choose, can add both reading and meaning
----Can do exercises separately from reading

--Full.8 Reading
----Same as above but will interrupt the reading to do exercises from time to time when reviews are advisable

--Full.6 Reading
----Same as above, but not automatic. Shows a phrase and asks if you want to proceed or add unknown word
----Readings could be per phrase and done after choosing unknown words, but think about that later

--Full.8 Learning
----Same as below, but can ignore meanings

--Full Learning
----Shows a phrase, introduces words and quizzes on both meaning and reading
----Go through all words to get a new phrase
----Can only get new words when reviews are done
----Can read collected story so far

BASIC FLOW:
Until all phrases are done
	Until all words of phrase:
		Introduce new word
		Do exercise
		Redo mistaken exercises
	Introduce phrase


BASIC DESIGN:
Original text => parts
for each part, we have valid words
For each word, we can generate exercises
