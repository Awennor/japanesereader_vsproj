﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader.analytics
{
    [Serializable]
    class WordAnalyticsData
    {
        public float Easeness { set; get; } = 1f;
        public int NextInteraction { set; get; }
        public int LastInteraction { set; get; }
        public int ExerciseProgress { get; set; }
        //public string AssociatedPhrase { set; get; } = null;
    }
}
