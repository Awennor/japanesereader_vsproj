﻿using JapaneseTextReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader.analytics
{
    [Serializable]
    class WordMiscData
    {
        public string FoundPhrase { get; set; }
        public WordMetaData WordMetaData { get; set; }
        
    }
}
