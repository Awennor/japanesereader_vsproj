﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JapaneseTextReader;
using pidroh.JapaneseTextReader.persistence;

namespace pidroh.JapaneseTextReader.analytics
{
    
    public class LearnerWordDataAccessor
    {
        internal Dictionary<string, WordAnalyticsData> WordDatas { get; set; }
        
        //internal Dictionary<string, WordMiscData> WordMiscDatas { get; set; } = new Dictionary<string, WordMiscData>();
        internal List<string> IgnoreWords { get; set; }

        private Dictionary<string, WordMiscData> miscDatas;
        internal int InteractionNumber;
        

        public LearnerWordDataAccessor(LearnerWordData_ExerciseTypeData meaningData)
        {
            ChangeData(meaningData);
        }

        public LearnerWordDataAccessor()
        {
        }

        public void ChangeData(LearnerWordData_ExerciseTypeData meaningData)
        {
            WordDatas = meaningData.WordDatas;
            IgnoreWords = meaningData.IgnoreWords;
            miscDatas = meaningData.WordMiscDatas;
        }

        public int config_FirstExerciseInterval { set; get; } = 8;
        public int config_MinCorrectInterval { set; get; } = 15;

        

        public int config_WrongExerciseInterval { set; get; } = 6;

        public bool ShouldReview()
        {
            foreach (var wd in WordDatas.Values)
            {
                if (wd.NextInteraction <= InteractionNumber)
                {
                    return true;
                }
            }
            return false;
        }


        public void ClearAllWordData()
        {
            WordDatas.Clear();
        }

        public string GetWordToReview()
        {
            string wordChosen = null;
            float highest = -999999;
            foreach (var pair in WordDatas)
            {
                var wd = pair.Value;
                var word = pair.Key;
                float wrongness = 1 - wd.Easeness;
                int interactionDiff = InteractionNumber - wd.NextInteraction;
                var wordReviewStrength = interactionDiff;
                if (highest < wordReviewStrength)
                {
                    highest = wordReviewStrength;
                    wordChosen = word;
                    //phrase = WordMiscDatas[word].FoundPhrase;
                }
            }
            return wordChosen;
        }

        internal WordMiscData GetWordMiscDataToReview()
        {
            string w = GetWordToReview();
            if (w == null) {
                return null;
            }
            WordMiscData wordMiscData = miscDatas[w];
            return wordMiscData;
        }

        internal void IgnoreWord(string word)
        {
            IgnoreWords.Add(word);
        }

        public bool IsKnownWord(string word)
        {

            return WordDatas.ContainsKey(word) || IgnoreWords.Contains(word);
        }


        public bool IsStudyingWord(string word)
        {

            return WordDatas.ContainsKey(word);
        }

        public void LearnNewWord(string word, string phrase = null)
        {
            if (!IsKnownWord(word))
            {
                miscDatas[word] = new WordMiscData();
                miscDatas[word].FoundPhrase = phrase;
                WordDatas[word] = new WordAnalyticsData();
                WordDatas[word].NextInteraction = InteractionNumber + config_FirstExerciseInterval;
                WordDatas[word].LastInteraction = InteractionNumber;
            }
        }

        internal void LearnNewWord(WordMetaData exercisedWMD, string phrase)
        {
            var word = exercisedWMD.DictionaryForm;
            LearnNewWord(word, phrase);
            miscDatas[word].WordMetaData = exercisedWMD;
           
            
        }



        public void Interacted()
        {
            InteractionNumber++;
        }



        public void ExercisedWord(string word, bool correctness)
        {
            float value = 0.9f;
            if (correctness) value = 1.1f;
            WordDatas[word].Easeness *= value;
            const float minDiff = 0.8f;
            if (WordDatas[word].Easeness < minDiff)
            {
                WordDatas[word].Easeness = minDiff;
            }

            //float previousValue = WordDatas[word].Difficulty;
            //WordDatas[word].Difficulty = (previousValue + value) / 2;
            int lastInteraction = WordDatas[word].LastInteraction;
            int oldInterval = InteractionNumber - lastInteraction;

            if (correctness)
            {
                ChangeProgress(word, 1);
                int newInterval = (int)(oldInterval * (WordDatas[word].Easeness + 1));
                if (newInterval < config_MinCorrectInterval)
                {
                    newInterval = config_MinCorrectInterval;
                }
                WordDatas[word].NextInteraction = InteractionNumber + newInterval;
            }
            else
            {
                ChangeProgress(word, -1);
                WordDatas[word].NextInteraction = InteractionNumber + config_WrongExerciseInterval;
            }

            WordDatas[word].LastInteraction = InteractionNumber;

        }

        private void ChangeProgress(string word, int v)
        {
            WordDatas[word].ExerciseProgress += v;
            if (WordDatas[word].ExerciseProgress < 0)
            {
                WordDatas[word].ExerciseProgress = 0;
            }
            if (WordDatas[word].ExerciseProgress > 3)
            {
                WordDatas[word].ExerciseProgress = 3;
            }
        }

        internal int GetExerciseProgress(string wordOfExercise)
        {
            return WordDatas[wordOfExercise].ExerciseProgress;
        }
    }
}
