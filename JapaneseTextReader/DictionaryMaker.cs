﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JapaneseTextReader
{
    class DictionaryMaker
    {

        List<string> aux = new List<string>();

        internal DictionaryData CreateJapaneseDictionaryEdict(string dictionaryText)
        {
            var data = new DictionaryData();

            StringBuilder sb = new StringBuilder();

            string[] lines = dictionaryText.Split('\n');
            foreach (var line in lines)
            {
                //System.Console.WriteLine(line);
                //MessageBox.Show(line);

                int firstBar = line.IndexOf('/');
                int secondBar = line.IndexOf('/', line.IndexOf('/') + 1);
                if (firstBar < 0) continue;
                var jpnLine = line.Substring(0, firstBar - 1);

                var words = GetJpnWords(jpnLine);
                var firstWord = words[0];
                for (int i = 1; i < words.Length; i++)
                {
                    data.AddDesambiguation(words[i], firstWord);
                }

                string[] readings = GetReadings(jpnLine);
                if (readings != null)
                {
                    foreach (var r in readings)
                    {
                        data.AddDesambiguation(r, firstWord);
                    }
                }


                var engLineRaw = line.Substring(firstBar + 1, secondBar - firstBar - 1);
                ExtractEnglishMeaning_NoParenthesis(sb, engLineRaw);
                var engMeaning = sb.ToString();
                //only gets first meaning for now
                var engMeanings = new String[] { engMeaning };

                data.AddMeanings(firstWord, engMeanings);
            }
            return data;
        }


        internal string PrintMeanings(DictionaryData dicData)
        {
            StringBuilder sb = new StringBuilder();
            dicData.PrintMeanings(sb);
            return sb.ToString();
        }

        private static void ExtractEnglishMeaning_NoParenthesis(StringBuilder sb, string engLineRaw)
        {
            sb.Length = 0;
            int paranthesisLevel = 0;
            for (int i = 0; i < engLineRaw.Length; i++)
            {
                char c = engLineRaw[i];
                bool par = false;
                if (c == '(' || c == '{')
                {
                    paranthesisLevel++;
                    par = true;
                }
                if (c == ')' || c == '}')
                {
                    par = true;
                    paranthesisLevel--;
                }
                if (!par && paranthesisLevel <= 0)
                {
                    if (sb.Length != 0 || c != ' ')
                        sb.Append(c);
                }
            }
        }

        private string[] GetJpnWords(string jpnLine)
        {
            jpnLine = jpnLine.Replace(" ", string.Empty);
            jpnLine = jpnLine.Replace("(p)", string.Empty);
            int id = jpnLine.IndexOf('[');
            if (id >= 0)
            {
                jpnLine = jpnLine.Substring(0, id);
            }
            string[] words = jpnLine.Split(';');
            for (int i = 0; i < words.Length; i++)
            {

                int pId = words[i].IndexOf('(');
                if (pId >= 0)
                {
                    words[i] = words[i].Substring(0, pId);
                }

            }
            return words;
        }

        private string[] GetReadings(string jpnLine)
        {
            int id = jpnLine.IndexOf('[');
            if (id >= 0)
            {
                int id2 = jpnLine.IndexOf(']');
                var jpnReadings = jpnLine.Substring(id + 1, id2 - id - 1).Split(';');
                return jpnReadings;
            }
            return null;
        }
    }
}
