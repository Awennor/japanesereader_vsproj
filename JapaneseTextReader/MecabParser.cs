﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JapaneseTextReader
{
    class MecabParser
    {

        List<WordMetaData> aux = new List<WordMetaData>();
        string[] ignoreOnMain = new string[] { "助詞", "記号", "助動詞" };
        string[] ignoreOnSecondary = new string[] {  };

        public MecabParser(Func<string, string> mecabProvider)
        {
            MecabProvider = mecabProvider;
        }

        //接尾
        public Func<string, string> MecabProvider { set; get; }

        public List<WordMetaData> Parse(string text)
        {
            return Parse(text, out string reading);
            //MessageBox.Show(originalMecab);
        }

        public List<WordMetaData> Parse(string text, out string reading)
        {
            aux.Clear();
            var stringList = aux;
            reading = Parse(text, stringList);

            return aux;
        }

        private string Parse(string text, List<WordMetaData> wordList)
        {
            string originalMecab = MecabProvider(text);
            string fullMecab = originalMecab.Replace('\t', ',');
            var lines = fullMecab.Split('\n');
            string reading = "";
            //MessageBox.Show(originalMecab);
            foreach (var l in lines)
            {
                if (l == "EOS") break;
                if (l.Length == 0) break;
                var parts = l.Split(',');
                if (parts.Length <= 7) continue;
                string main = parts[0];
                string originalForm = parts[7];
                string wordReading = "";
                if (parts.Length > 8) {
                    wordReading = parts[8];
                    reading += wordReading;
                    
                }
                    

                //bool ignoreB = false;
                var listToIgnore = ignoreOnMain;
                bool ignoreB = TestIgnoreList(parts[1], listToIgnore);
                if (!ignoreB)
                    ignoreB = TestIgnoreList(parts[2], ignoreOnSecondary);
                if (!ignoreB)
                    ignoreB = testIfNumber(originalForm);
                //if (ignoreB) MessageBox.Show(originalForm + " was removed");
                if (ignoreB) continue;

                if (originalForm.Length > 0 && originalForm != "*")
                {
                    wordList.Add(new WordMetaData(main, wordReading, originalForm));
                    //MessageBox.Show(originalForm);
                }
            }
            return reading;
        }

        private string Parse(string text, List<string> stringList)
        {
            string originalMecab = MecabProvider(text);
            var lines = originalMecab.Split('\n');
            string reading = "";
            //MessageBox.Show(originalMecab);
            foreach (var l in lines)
            {
                if (l == "EOS") break;
                if (l.Length == 0) break;
                var parts = l.Split(',');
                if (parts.Length <= 6) continue;
                string main = parts[0];
                string originalForm = parts[6];
                if (parts.Length > 7)
                    reading += parts[7];

                //bool ignoreB = false;
                var listToIgnore = ignoreOnMain;
                bool ignoreB = TestIgnoreList(main, listToIgnore);
                if (!ignoreB)
                    ignoreB = TestIgnoreList(parts[1], ignoreOnSecondary);
                if (!ignoreB)
                    ignoreB = testIfNumber(originalForm);
                //if (ignoreB) MessageBox.Show(originalForm + " was removed");
                if (ignoreB) continue;

                if (originalForm.Length > 0 && originalForm != "*")
                {
                    stringList.Add(originalForm);
                    //MessageBox.Show(originalForm);
                }
            }
            return reading;
        }

        private bool testIfNumber(string originalForm)
        {
            if (originalForm.Length == 1)
            {
                char c = originalForm[0];
                if (c >= '０' && c <= '９')
                {
                    return true;
                }
            }
            return false;
        }

        private static bool TestIgnoreList(string main, string[] listToIgnore)
        {
            bool ignoreB = false;
            foreach (var og in listToIgnore)
            {
                if (main.Contains(og))
                {
                    ignoreB = true;
                    break;
                }
            }

            return ignoreB;
        }
    }
}
