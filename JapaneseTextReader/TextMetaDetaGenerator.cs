﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JapaneseTextReader
{
    class TextMetaDetaGenerator
    {

        DictionaryData dd;
        MecabParser parser;
        char[] separators = { '.', '。', '\n' };
        List<string> linesAux = new List<string>();

        public TextMetaDetaGenerator(DictionaryData dd, MecabParser parser)
        {
            this.dd = dd;
            this.parser = parser;
        }

        public TextMetaData Generate(string text, string title = null)
        {
            TextMetaData tmd = new TextMetaData(title);
            GetLines(text);
            var lines = linesAux;
            foreach (var l in lines)
            {
                TextSegment segment = new TextSegment(l);
                tmd.Segments.Add(segment);
                var words = parser.Parse(l, out string reading);
                segment.SegmentReading = reading;

                foreach (var w in words)
                {
                    if (dd.GetMeaning(w.DictionaryForm) != null)
                    {
                        segment.ValidWords.Add(w);
                    }
                    else
                    {
                        //MessageBox.Show("Word not found "+w);
                    }
                }



            }

            return tmd;
        }

        private void GetLines(string text)
        {
            linesAux.Clear();
            int lineStart = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (separators.Contains(text[i]))
                {
                    int lineEnd = i;
                    int length = lineEnd - lineStart +1;
                    if (length > 1)
                    {
                        linesAux.Add( text.Substring(lineStart, length));
                        lineStart = lineEnd + 1;
                    }

                }
            }
        }
    }
}
