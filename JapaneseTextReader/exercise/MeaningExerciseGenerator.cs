﻿using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JapaneseTextReader.exercise
{
    class MeaningExerciseGenerator
    {

        DictionaryData dictionaryData;
        List<int> wordLengths = new List<int>();
        int minPartLengthDesired;
        PartChoiceExerciseGenerator generator = new PartChoiceExerciseGenerator();

        public MeaningExerciseGenerator(DictionaryData dictionaryData)
        {
            this.dictionaryData = dictionaryData;
        }

        public PartChoiceExercise GenerateReviewExercise(string word, string meaning, LearnerWordDataAccessor profile)
        {
            AddDummies(profile);
            var exercise = generator.Generate(word, meaning);
            return exercise;
        }

        public PartChoiceExercise GenerateReviewExerciseReversed(string word, string meaning, LearnerWordDataAccessor profile, TextMetaData textMetaData, string phrase)
        {
            AddDummies_Meaning(profile);

            var segs = textMetaData.Segments;

            int amountOfDummies = 0;
            for (int i = 0; i < segs.Count && (amountOfDummies < 10); i++)
            {
                var segment = segs[i];
                for (int j = 0; j < segment.ValidWords.Count; j++)
                {
                    var validWord = segment.ValidWords[j].DictionaryForm;
                    validWord = dictionaryData.GetMeaning(validWord);
                    if (validWord != meaning)
                    {
                        generator.AddDummy(validWord);
                        amountOfDummies++;
                    }

                }
            }

            var exercise = generator.Generate(meaning, word);
            exercise.ContextPhrase = phrase;
            return exercise;
        }

        private void AddDummies(LearnerWordDataAccessor profile)
        {
            generator.ClearDummies();
            foreach (var item in profile.WordDatas.Keys)
            {
                generator.AddDummy(item);
            }
        }

        private void AddDummies_Meaning(LearnerWordDataAccessor profile)
        {
            generator.ClearDummies();
            foreach (var item in profile.WordDatas.Keys)
            {
                generator.AddDummy(dictionaryData.GetMeaning(item));
            }
        }

        public PartChoiceExercise GenerateReviewExercise(string word, string meaning, LearnerWordDataAccessor profile, TextMetaData textMetaData)
        {
            AddDummies(profile);

            var segs = textMetaData.Segments;


            int amountOfDummies = 0;
            for (int i = 0; i < segs.Count && (amountOfDummies < 10); i++)
            {
                var segment = segs[i];
                for (int j = 0; j < segment.ValidWords.Count; j++)
                {
                    var validWord = segment.ValidWords[j];
                    if (validWord.DictionaryForm != word)
                    {
                        generator.AddDummy(validWord.DictionaryForm);
                        amountOfDummies++;
                    }

                }
            }

            var exercise = generator.Generate(word, meaning);
            return exercise;
        }

        public PartChoiceExercise GenerateNewWordExercise(TextMetaData textMetaData, int segmentId, int wordId)
        {
            generator.ClearDummies();
            
            var segs = textMetaData.Segments;
            var word = segs[segmentId].ValidWords[wordId];
            var meaning = dictionaryData.GetMeaning(word.DictionaryForm);


            int amountOfDummies = 0;
            for (int i = 0; i < segs.Count && (i <= segmentId || amountOfDummies < 10); i++)
            {
                var segment = segs[i];
                for (int j = 0; j < segment.ValidWords.Count; j++)
                {
                    var validWord = segment.ValidWords[j];
                    if (validWord != word) {
                        generator.AddDummy(validWord.DictionaryForm);
                        amountOfDummies++;
                    }
                        
                }
            }

            var exercise = generator.Generate(word.DictionaryForm, meaning);



            return exercise;
        }







    }
}
