﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JapaneseTextReader.exercise
{
    public class PartChoiceExercise
    {
        public string Prompt { get; set; }
        public string Answer { get; set; }
        public List<string> AnswerParts { get; } = new List<string>();
        public List<List<string>> Dummies { get; } = new List<List<string>>();
        public List<int> ShownParts { get; set; } = new List<int>();
        public string ContextPhrase { get; set; }

        public PartChoiceExercise(string prompt, string answer)
        {
            this.Prompt = prompt;
            this.Answer = answer;
        }
    }
}
