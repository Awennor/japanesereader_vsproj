﻿using JapaneseTextReader.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader.exercise
{
    class PartChoiceExerciseGenerator
    {
        List<string> DummyCandidates { set; get; } = new List<string>();
        List<int> wordLengths = new List<int>();

        public PartChoiceExercise Generate(string answer, string prompt, int difficulty = 99)
        {
            PartChoiceExercise exercise = new PartChoiceExercise(prompt, answer);
            int numberOfParts = DecideNumberOfParts(answer);
            if (difficulty < 2) {
                exercise.ShownParts.Add(0);
            }
            if (difficulty == 0 && numberOfParts > 2)
            {
                exercise.ShownParts.Add(1);
            }
            //exercise.ShownParts.Add(0);
            DivideIntoParts(answer, exercise.AnswerParts, numberOfParts);

            GenerateDummies(exercise, numberOfParts);
            return exercise;
        }

        private void GenerateDummies(PartChoiceExercise exercise, int numberOfParts)
        {
            int amountOfDummies = 5;
            while (exercise.Dummies.Count < numberOfParts)
            {
                exercise.Dummies.Add(new List<string>());
            }

            for (int part = 0; part < numberOfParts; part++)
            {
                int dummiesGenerated = 0;
                for (int i = 0; i < DummyCandidates.Count; i++)
                {

                    var word = DummyCandidates[i];
                    string dummy = GetPartForDummy(word, numberOfParts, part);
                    if (exercise.Dummies[part].Contains(dummy) || dummy == exercise.AnswerParts[part])
                    {
                        continue;
                    }
                    exercise.Dummies[part].Add(dummy);
                    dummiesGenerated++;
                    if (dummiesGenerated == amountOfDummies)
                    {
                        break;
                    }
                }
            }
        }

        internal void ClearDummies()
        {
            DummyCandidates.Clear();
        }

        internal void AddDummy(string validWord)
        {
            DummyCandidates.Add(validWord);
        }

        private string GetPartForDummy(string word, int numberOfPartsOfOriginalWord, int partPosition)
        {
            int numberOfParts_Dummy = DecideNumberOfParts(word);
            if (numberOfParts_Dummy == 1)
            {
                return word;
            }
            int partToReturn = partPosition;
            if (partToReturn >= numberOfParts_Dummy)
                partToReturn = numberOfParts_Dummy - 1;

            CalculatePartLengths(word, numberOfParts_Dummy);
            int offset = 0;
            for (int i = 0; i < numberOfParts_Dummy; i++)
            {
                int length = wordLengths[i];

                if (i == partToReturn)
                {

                    return word.Substring(offset, length);
                }
                else
                {
                }
                offset += length;
            }
            return null;
        }

        private int DecideNumberOfParts(string word)
        {
            if (word.Length == 1) return 1;
            if (word.Length == 2) return 2;
            return 3;
        }

        private void DivideIntoParts(string word, List<string> list, int numberOfParts)
        {
            if (numberOfParts == 1)
            {
                list.Add(word);
                return;
            }
            CalculatePartLengths(word, numberOfParts);
            int offset = 0;
            for (int i = 0; i < numberOfParts; i++)
            {
                int length = wordLengths[i];
                list.Add(word.Substring(offset, length));
                offset += length;
            }
        }

        private void CalculatePartLengths(string word, int numberOfParts)
        {
            wordLengths.Clear();
            float length = word.Length;
            int floor = (int)Math.Floor(length / numberOfParts);
            int fullLength = 0;
            for (int i = 0; i < numberOfParts; i++)
            {
                int pLength = floor;
                if (i < length % numberOfParts)
                    pLength += 1;
                fullLength += pLength;
                wordLengths.Add(pLength);
            }
        }
    }
}
