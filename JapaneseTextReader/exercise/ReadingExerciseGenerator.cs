﻿using pidroh.JapaneseTextReader;
using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JapaneseTextReader.exercise
{
    class ReadingExerciseGenerator
    {

        DictionaryData dictionaryData;
        List<int> wordLengths = new List<int>();
        int minPartLengthDesired;
        PartChoiceExerciseGenerator generator = new PartChoiceExerciseGenerator();

        public ReadingExerciseGenerator(DictionaryData dictionaryData)
        {
            this.dictionaryData = dictionaryData;
        }

        public PartChoiceExercise GenerateExercise(string word, string reading_Katakana, LearnerWordDataAccessor profile, TextMetaData textMetaData, string phrase)
        {
            var readingHira = JapaneseUtils.KatakanaToHiragana(reading_Katakana);
            Trimming(readingHira,word, out string word_Out, out string reading_Out);
            readingHira = reading_Out;
            word = word_Out;
            AddDummies(profile);


            var segs = textMetaData.Segments;

            int amountOfDummies = 0;
            for (int i = 0; i < segs.Count && (amountOfDummies < 10); i++)
            {
                var segment = segs[i];
                for (int j = 0; j < segment.ValidWords.Count; j++)
                {
                    var dummyReading_Hira = JapaneseUtils.KatakanaToHiragana(segment.ValidWords[j].Reading);
                    if (dummyReading_Hira != readingHira)
                    {
                        generator.AddDummy(dummyReading_Hira);
                        amountOfDummies++;
                    }

                }
            }

            var exercise = generator.Generate(readingHira, word);
            exercise.ContextPhrase = phrase;
            return exercise;
        }

        private void Trimming(string readingHira, string word, out string wordOut, out string readingOut)
        {
            wordOut = word;
            readingOut = readingHira;
            var removeFromStart = 0;
            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] != readingHira[i])
                    break;
                removeFromStart++;
            }
            var removeFromEnd = 0;
            for (int i = 0; i < word.Length; i++)
            {
                char wordChar = word[word.Length-i-1];
                char readingChar = readingHira[readingHira.Length-i-1];
                if (wordChar != readingChar)
                    break;
                removeFromEnd++;
            }
            Console.Write(removeFromEnd+"__"+removeFromStart);
            if (removeFromStart == 0 && removeFromEnd == 0) {
                return;
            }
            wordOut = word.Substring(removeFromStart, word.Length - removeFromStart-removeFromEnd);
            readingOut = readingHira.Substring(removeFromStart, readingHira.Length - removeFromStart - removeFromEnd);
        }

        private void AddDummies(LearnerWordDataAccessor profile)
        {
            
        }
    }
}
