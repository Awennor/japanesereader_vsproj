﻿using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader
{
    public class WordListLearnerNavigator
    {
        private WordListLearner wll;
        private LearnerWordDataAccessor learnerProfile;
        States state;
        int whichWord;
        private string wordOfExercise;
        private PartChoiceExercise exercise;

        public event Action<string, string> OnIntroduceWordIntent;
        public event Action<PartChoiceExercise> OnExerciseWordIntent;


        public WordListLearnerNavigator()
        {
            wll = new WordListLearner();
            learnerProfile = new LearnerWordDataAccessor();
        }

        public void Start() {
            state = States.newWord;
            ActionOfState();
        }

        public void Initialize(string[] words, string[] meanings)
        {
            wll.InitializeDictionary(words, meanings);
            wll.AddWordsToLearn(words);
        }

        public void ExerciseReport(bool success)
        {
            learnerProfile.ExercisedWord(wordOfExercise, success);
            learnerProfile.Interacted();

        }

        public void Advance()
        {
            switch (state)
            {
                case States.newWord:
                    state = States.exerciseNewWord;
                    break;
                case States.exerciseNewWord:
                    state = States.checkReviews;
                    break;
                //case States.checkReviews:
                //    break;
                case States.review:
                    state = States.checkReviews;
                    break;
                default:
                    break;
            }
            ActionOfState();
        }

        private void ActionOfState()
        {
            switch (state)
            {
                case States.newWord:

                    var word = wll.WordsToLearn[whichWord];
                    learnerProfile.LearnNewWord(word);
                    learnerProfile.Interacted();
                    
                    var trans = wll.Dictionary.GetMeaning(word);
                    OnIntroduceWordIntent(word, trans);
                    //var ex = wll.GetExercise(word);
                    break;
                case States.exerciseNewWord:
                    {
                        var w = wll.WordsToLearn[whichWord];
                        whichWord++;
                        ExerciseWord(w);
                    }

                    break;
                case States.checkReviews:
                    if (learnerProfile.ShouldReview())
                    {
                        state = States.review;
                    }
                    else
                    {
                        state = States.newWord;
                    }
                    ActionOfState();
                    break;
                case States.review:
                    ExerciseWord(learnerProfile.GetWordToReview());
                    break;
                default:
                    break;
            }
        }

        private void ExerciseWord(string w)
        {
            wordOfExercise = w;
            int progress = learnerProfile.GetExerciseProgress(wordOfExercise);
            int difficulty = progress - 1;
            if (progress == 0) difficulty = 5;
            exercise = wll.GetExercise(w, difficulty);
            OnExerciseWordIntent(exercise);
        }

        private enum States
        {
            newWord, exerciseNewWord, checkReviews, review
        }
    }

}
