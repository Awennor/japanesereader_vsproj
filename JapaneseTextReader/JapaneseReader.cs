﻿using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.persistence;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace JapaneseTextReader
{
    [Serializable]
    public class JapaneseReader
    {

        TextMetaData textMetaData;
        internal MeaningExerciseGenerator ExerciseGenerator;
        internal DictionaryData DicData;

        public int PhrasesProgress { get; private set; } = 0;

        public SegmentWordAdvancer Advancer { get; private set; }

        public int NumberOfSegments
        {
            get
            {
                return textMetaData.Segments.Count;
            }

        }

        internal TextMetaData TextMetaData { get => textMetaData; set => textMetaData = value; }

        public bool CanLearnPhrase()
        {
            return Advancer.CurrentSegment > PhrasesProgress;
        }

        public int GetNumberOfWords(int seg)
        {
            return textMetaData.Segments[seg].ValidWords.Count;
        }

        public string GetWord()
        {
            return textMetaData.Segments[Advancer.CurrentSegment].ValidWords[Advancer.CurrentWord].DictionaryForm;
        }

        internal void ChangeStory(TextMetaData tmd, TextProgress textProgress)
        {
            this.textMetaData = tmd;
            Advancer.ChangeText(tmd, textProgress);
        }

        public string GetTranslation(int currentSeg, int currentWord)
        {
            var word = textMetaData.Segments[currentSeg].ValidWords[currentWord];
            return DicData.GetMeaning(word.DictionaryForm);
        }

        internal PartChoiceExercise GetExercise()
        {
            return GetExercise(Advancer.CurrentSegment, Advancer.CurrentWord);
        }

        public string GetTranslation(string word)
        {

            return DicData.GetMeaning(word);
        }


        public JapaneseReader(string dictionaryText, string storyText, Func<string, string> mecab, TextProgress progress = null)
        {


            var dicMake = new DictionaryMaker();
            DicData = dicMake.CreateJapaneseDictionaryEdict(dictionaryText);
            var textMetaDetaGenerator = new TextMetaDetaGenerator(DicData, new MecabParser(mecab));

            

            textMetaData = textMetaDetaGenerator.Generate(storyText);
            ExerciseGenerator = new MeaningExerciseGenerator(DicData);

            Advancer = new SegmentWordAdvancer(textMetaData, progress);
        }

        public JapaneseReader()
        {
            Advancer = new SegmentWordAdvancer();
        }

        internal string CurrentPhrase()
        {
            return GetSegmentText(Advancer.CurrentSegment);
        }

        internal string GetSegmentText(int i)
        {
            return textMetaData.Segments[i].Segment;
        }

        internal string LearnNewPhrase()
        {
            string segment = textMetaData.Segments[PhrasesProgress].Segment;
            PhrasesProgress++;
            return segment;

        }

        public PartChoiceExercise GetExercise(int seg, int word)
        {
            return ExerciseGenerator.GenerateNewWordExercise(textMetaData, seg, word);
        }

        public PartChoiceExercise GetReviewExercise(string word, LearnerWordDataAccessor profile, string phrase)
        {
            return ExerciseGenerator.GenerateReviewExerciseReversed(word, GetTranslation(word), profile, textMetaData, phrase);
        }
    }
}
