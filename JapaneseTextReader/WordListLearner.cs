﻿using JapaneseTextReader;
using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader
{
    public class WordListLearner
    {
        private List<string> wordsToLearn = new List<string>();
        internal DictionaryData Dictionary { get; set; }
        PartChoiceExerciseGenerator exerciseGen = new PartChoiceExerciseGenerator();

        public WordListLearner()
        {
        }

        public void InitializeDictionary(string[] words, string[] meanings) {
            Dictionary = new DictionaryData();
            for (int i = 0; i < words.Length; i++)
            {
                Dictionary.AddMeanings(words[i], new string[] { meanings[i]});
            }

        }

        public List<string> WordsToLearn { get => wordsToLearn; set => wordsToLearn = value; }

        public void AddWordToLearn(string word) {
            if (Dictionary.HasMeaning(word)) {
                WordsToLearn.Add(word);
            }
        }

        public void AddWordsToLearn(string[] words)
        {
            WordsToLearn.AddRange(words);
        }

        public PartChoiceExercise GetExercise(int wordId)
        {
            string word = wordsToLearn[wordId];

            return GetExercise(word);
        }

        public PartChoiceExercise GetExercise(string word, int difficulty = 99)
        {
            var meaning = Dictionary.GetMeaning(word);
            exerciseGen.ClearDummies();
            foreach (var w in wordsToLearn)
            {
                exerciseGen.AddDummy(w);
            }
            return exerciseGen.Generate(word, meaning, difficulty);
        }


    }
}
