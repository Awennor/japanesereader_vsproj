﻿using JapaneseTextReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader
{
    public class TextFactory
    {
        private MecabParser mecab;
        DictionaryMaker dicmake = new DictionaryMaker();
        private DictionaryData dictionaryData;
        TextMetaDataHolder texts = new TextMetaDataHolder();
        private TextMetaDetaGenerator textMetaGen;

        public TextMetaData CreateTextMeta(string title, string storyText)
        {
            var meta = textMetaGen.Generate(storyText, title);
            return meta;
        }

        public void Setup(Func<string, string> value, string edictDictionary)
        {
            
            SetMecabFunction(value);
            LoadEdictDictionary(edictDictionary);
            textMetaGen = new TextMetaDetaGenerator(dictionaryData, mecab);
            
        }

        private void SetMecabFunction(Func<string, string> value)
        {
            mecab = new MecabParser(value);
        }

        private void LoadEdictDictionary(string edictDicText)
        {
            dictionaryData = dicmake.CreateJapaneseDictionaryEdict(edictDicText);
        }
    }
}
