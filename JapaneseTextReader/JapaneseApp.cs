﻿using JapaneseTextReader;
using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader.persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader
{
    public class JapaneseApp
    {
        public LearnerData LearnerData
        {
            get; private set;
        }
        public JapaneseReaderSuper Jsr { get; private set; }

        DictionaryMaker dicmake = new DictionaryMaker();
        private DictionaryData dictionaryData;
        private MeaningExerciseGenerator exGen;
        private MecabParser mecab;
        TextMetaDataHolder texts = new TextMetaDataHolder();
        private TextMetaDetaGenerator textMetaGen;
        public ReadingExerciseManager ReadingExerciseManager { private set;  get; }

        public void AddStory(string title, string storyText)
        {
            var meta = textMetaGen.Generate(storyText, title);
            AddStory(meta);

        }

        public void AddStory(TextMetaData meta)
        {
            texts.Add(meta);
        }

        public void ChooseStory(int story)
        {
            var tmd = texts.Texts[story];
            Jsr.ChangeStory(tmd, LearnerData.LearnerTextProgress);
        }

        public void ChooseStory(string title)
        {

            foreach (var t in texts.Texts)
            {
                if (t.Title == title)
                {
                    ChooseStory(texts.Texts.IndexOf(t));
                }
            }
        }

        public float GetStoryProgress(string title)
        {
            TextProgress prog = LearnerData.LearnerTextProgress.GetProgress(title);
            if (prog == null) return -1;
            float seg = prog.GetProgress(1)+1;
            if (!prog.GoalProgress.ContainsKey(1)) return 0;
            float maxSeg = prog.GoalProgress[1];
            if (maxSeg == 0) return 0;
            return seg / maxSeg;
        }

        public bool HasStory(string title)
        {

            foreach (var t in texts.Texts)
            {
                if (t.Title == title)
                {
                    return true;
                }
            }
            return false;
        }

        private void SetupLearnerData(LearnerData learnerData)
        {

            ChangeLearnerData(learnerData);

        }

        public void ChangeLearnerData(LearnerData learnerData)
        {
            this.LearnerData = learnerData;
            Jsr.ChangeLearnerData(learnerData.learnerWordData.MeaningData);
        }

        public PartChoiceExercise GetNewReadingExercise() {
            var currentSegmentData = Jsr.GetCurrentSegmentData();
            
            for (int i = 0; i < currentSegmentData.ValidWords.Count; i++)
            {
                var wmd = currentSegmentData.ValidWords[i];
                if (ReadingExerciseManager.ShouldLearnWordReading(wmd)) {
                    var ex = ReadingExerciseManager.GetExercise(wmd, Jsr.Jr.TextMetaData, Jsr.GetCurrentPhrase());
                    return ex;
                }
            }
            return null;
            
        }



        public PartChoiceExercise GetReadingReviewExercise()
        {

            var ex = ReadingExerciseManager.GetReviewExercise(Jsr.Jr.TextMetaData);
            return ex;
        }

        public void Setup(Func<string, string> value, string edictDictionary, LearnerData learnerData)
        {
            Jsr = new JapaneseReaderSuper();
            SetMecabFunction(value);
            LoadEdictDictionary(edictDictionary);
            textMetaGen = new TextMetaDetaGenerator(dictionaryData, mecab);
            SetupLearnerData(learnerData);
            Jsr.Jr.DicData = this.dictionaryData;
            Jsr.Jr.ExerciseGenerator = this.exGen;
            ReadingExerciseManager = new ReadingExerciseManager();
            ReadingExerciseManager.Setup(dictionaryData, learnerData.learnerWordData.ReadingData);
        }

        private void SetMecabFunction(Func<string, string> value)
        {
            mecab = new MecabParser(value);
        }

        private void LoadEdictDictionary(string edictDicText)
        {
            dictionaryData = dicmake.CreateJapaneseDictionaryEdict(edictDicText);
            exGen = new MeaningExerciseGenerator(dictionaryData);
        }




    }
}
