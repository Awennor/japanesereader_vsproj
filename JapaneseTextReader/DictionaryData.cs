﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JapaneseTextReader
{
    class DictionaryData
    {
        Dictionary<string, string> desambiguationDic = new Dictionary<string, string>();
        Dictionary<string, string[]> meanings = new Dictionary<string, string[]>();

        internal void AddDesambiguation(string v, string firstWord)
        {
            if (!desambiguationDic.ContainsKey(v))
                desambiguationDic.Add(v, firstWord);
        }

        internal void AddMeanings(string firstWord, string[] engMeaning)
        {
            if (meanings.ContainsKey(firstWord))
            {
                //Console.WriteLine("DUPLICATE FIRST WORD " + firstWord);
            }
            else
            {
                meanings.Add(firstWord, engMeaning);
            }

        }

        internal string GetMeaning(string t)
        {
            if (meanings.ContainsKey(t)) {
                return meanings[t][0];
            }
            if (desambiguationDic.ContainsKey(t))
            {
                string des = desambiguationDic[t];
                return meanings[des][0];
            }
            return null;
            
        }

        internal bool HasMeaning(string t)
        {
            return meanings.ContainsKey(t);

            
        }

        internal void PrintMeanings(StringBuilder sb)
        {
            foreach (var pair in meanings)
            {
                
                sb.Append(pair.Key);
                sb.Append(':');
                foreach (var m in pair.Value)
                {
                    sb.Append(m);
                    sb.Append('/');
                }
                sb.Append('\n');
            }
        }
    }
}
