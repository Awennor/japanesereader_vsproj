﻿using JapaneseTextReader;
using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader
{
    public class ReadingExerciseManager
    {
        private LearnerWordDataAccessor dataAccessor;
        ReadingExerciseGenerator exGen;
        private WordMetaData exercisedWMD;
        private string exercisedPhrase;

        internal void Setup(DictionaryData dictionaryData, LearnerWordData_ExerciseTypeData readingData)
        {
            dataAccessor = new LearnerWordDataAccessor(readingData);
            exGen = new ReadingExerciseGenerator(dictionaryData);
        }

        public bool ShouldReview() {
            return dataAccessor.ShouldReview();
        }

        public void ReportOnExercise(bool correct)
        {
            if (dataAccessor.IsKnownWord(exercisedWMD.DictionaryForm))
            {
                //if (dataAccessor.IsStudyingWord(exercisedWMD.DictionaryForm))
                    dataAccessor.ExercisedWord(exercisedWMD.DictionaryForm, correct);
            }
            else
            {
                if (correct)
                {
                    dataAccessor.IgnoreWord(exercisedWMD.DictionaryForm);
                }
                else
                {
                    dataAccessor.LearnNewWord(exercisedWMD, exercisedPhrase);
                }
            }

        }

        internal bool ShouldLearnWordReading(WordMetaData wmd)
        {
            if (dataAccessor.IsKnownWord(wmd.DictionaryForm))
            {
                return false;
            }
            var reading = wmd.Reading;
            var word = wmd.Word;
            if (word.Length != reading.Length)
                return true;
            for (int i = 0; i < word.Length; i++)
            {
                char hira = (char)(reading[i] - 'ア' + 'あ');
                if (word[i] != reading[i] && word[i] != hira)
                {
                    return true;
                }
            }
            return false;
        }

        internal PartChoiceExercise GetExercise(WordMetaData wmd, TextMetaData textMetaData, string phrase)
        {

            this.exercisedWMD = wmd;
            this.exercisedPhrase = phrase;
            return exGen.GenerateExercise(wmd.Word, wmd.Reading, dataAccessor, textMetaData, phrase);
        }

        internal PartChoiceExercise GetReviewExercise(TextMetaData textMetaData)
        {
            WordMiscData wordMiscData = dataAccessor.GetWordMiscDataToReview();
            if (wordMiscData == null) return null;
            var wmd = wordMiscData.WordMetaData;
            return exGen.GenerateExercise(wmd.Word, wmd.Reading, dataAccessor, textMetaData, wordMiscData.FoundPhrase);

        }
    }

}
