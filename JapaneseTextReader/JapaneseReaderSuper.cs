﻿using JapaneseTextReader;
using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader
{
    [Serializable]
    public class JapaneseReaderSuper
    {
        private JapaneseReader jr;
        private LearnerWordDataAccessor learnerWD = new LearnerWordDataAccessor();

        [NonSerialized]
        private string word_Exercising;

        List<string> aux = new List<string>();

        public int PhraseLearnedAmount { get => jr.PhrasesProgress; }
        internal LearnerWordDataAccessor meaningExerciseData { get => learnerWD; set => learnerWD = value; }
        internal JapaneseReader Jr { get => jr; set => jr = value; }

        public JapaneseReaderSuper(string storyText, string title, string dictionaryText, Func<string, string> parse, LearnerData learner)
        {
            jr = new JapaneseReader(dictionaryText, storyText, parse, learner.LearnerTextProgress.GetProgress(title));
            this.learnerWD = new LearnerWordDataAccessor(learner.learnerWordData.MeaningData);
        }

        internal void ChangeStory(TextMetaData tmd, LearnerTextProgress learnerTextProgress)
        {
            jr.ChangeStory(tmd, learnerTextProgress.GetProgress(tmd.Title));
        }

        public JapaneseReaderSuper()
        {
            jr = new JapaneseReader();
        }

        public string LearnNewWord()
        {
            string word = GetWordForLearning();

            learnerWD.LearnNewWord(word, GetCurrentPhrase());
            learnerWD.Interacted();
            return word;
        }
        public void IgnoreNewWord()
        {
            string word = GetWordForLearning();
            learnerWD.IgnoreWord(word);
            FinishedNewWord();
        }

        public void FinishedNewWord()
        {
            jr.Advancer.Advance();
        }

        public int GetCurrentSegment()
        {
            return jr.Advancer.CurrentSegment;
        }

        public string GetWordForLearning()
        {
            string word = jr.GetWord();
            while (learnerWD.IsKnownWord(word))
            {
                jr.Advancer.Advance();
                word = jr.GetWord();
            }

            return word;
        }

        public void AdvanceSegment()
        {
            jr.Advancer.AdvanceSegment();
            learnerWD.Interacted();
        }

        internal void ChangeLearnerData(LearnerWordData_ExerciseTypeData meaningData)
        {
            learnerWD.ChangeData(meaningData);
        }

        public bool CanAdvanceSegment()
        {
            return jr.Advancer.CanAdvanceSegment();
        }

        public string GetTranslation(string wordToLearn)
        {
            return jr.GetTranslation(wordToLearn);
        }

        internal TextSegment GetCurrentSegmentData()
        {
            return jr.TextMetaData.Segments[GetCurrentSegment()];
        }

        public PartChoiceExercise NewWordExercise()
        {
            word_Exercising = jr.GetWord();
            return jr.GetExercise();
        }

        public PartChoiceExercise ReviewExercise()
        {
            word_Exercising = learnerWD.GetWordToReview();
            
            return jr.GetReviewExercise(word_Exercising, learnerWD, null);
        }

        public bool KnownWord(string wordNew)
        {
            return learnerWD.IsKnownWord(wordNew);
        }
        
        public void LearnWord(string word, string phrase)
        {
            learnerWD.LearnNewWord(word, phrase);
            learnerWD.Interacted();
        }

        public void ExerciseResult(bool obj)
        {
            learnerWD.ExercisedWord(word_Exercising, obj);
        }



        public bool CanLearnNewPhrase()
        {
            return jr.CanLearnPhrase();
        }

        public bool CanReview()
        {
            return learnerWD.WordDatas.Count > 0;
        }

        public string LearnNewPhrase()
        {
            return jr.LearnNewPhrase();
        }

        public string GetPhrase(int i)
        {
            return jr.GetSegmentText(i);
        }

        public List<string> GetWords(int segment)
        {
            aux.Clear();
            foreach (var w in jr.TextMetaData.Segments[segment].ValidWords)
            {
                aux.Add(w.DictionaryForm);
            }
            return aux;
        }

        public bool ShouldReview()
        {
            return learnerWD.ShouldReview();
        }

        public string GetCurrentPhrase()
        {
            return jr.CurrentPhrase();

        }


    }
}
