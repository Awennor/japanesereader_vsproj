﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JapaneseTextReader
{
    [Serializable]
    public class TextMetaData
    {
        public TextMetaData(string title)
        {
            Title = title;
        }

        public string Title { get; set; }
        internal List<TextSegment> Segments { get; set; } = new List<TextSegment>();
    }
    [Serializable]
    class TextSegment
    {
        public string Segment { set; get; }
        public string SegmentReading { set; get; }
        public List<WordMetaData> ValidWords { set; get; } = new List<WordMetaData>();

        public TextSegment(string segment)
        {
            this.Segment = segment;
        }
    }
    [Serializable]
    internal class WordMetaData {
        public WordMetaData(string word, string reading, string dictionaryForm)
        {
            Word = word;
            Reading = reading;
            DictionaryForm = dictionaryForm;
        }

        public string Word { set; get; }
        public string Reading { set; get; }
        public string DictionaryForm { set; get; }
    }
}
