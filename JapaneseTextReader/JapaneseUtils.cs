﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.JapaneseTextReader
{
    static class JapaneseUtils
    {

        static StringBuilder aux = new StringBuilder();

        public static string KatakanaToHiragana(string katakana) {
            aux.Length = 0;
            for (int i = 0; i < katakana.Length; i++)
            {
                aux.Append((char)(katakana[i]-'ア'+'あ'));
            }
            return aux.ToString();
        }
    }
}
