﻿using pidroh.JapaneseTextReader.persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JapaneseTextReader
{
    [Serializable]
    public class SegmentWordAdvancer
    {
        [NonSerialized]
        TextMetaData textMetaData;

        TextProgress textProgress;

        internal SegmentWordAdvancer(TextMetaData textMetaData, TextProgress textProgress = null)
        {
            ChangeText(textMetaData, textProgress);
            UpdateMax();
        }

        internal void ChangeText(TextMetaData textMetaData, TextProgress textProgress)
        {
            this.textMetaData = textMetaData;
            this.textProgress = textProgress;
        }

        public SegmentWordAdvancer()
        {
        }

        public int CurrentSegment { get => textProgress.GetProgress(1); set => textProgress.SetProgress(1, value); }
        public int CurrentWord { get => textProgress.GetProgress(0); set => textProgress.SetProgress(0, value); }



        public bool IsOver()
        {
            if (NumberOfSegments() <= CurrentSegment)
            {
                return true;
            }
            return false;
        }

        public void Advance()
        {
            CurrentWord++;
            if (GetNumberOfWords(CurrentSegment) <= CurrentWord)
            {
                AdvanceSegment();
            }
            UpdateMax();
        }

        private void UpdateMax()
        {
            textProgress.GoalProgress[1] = NumberOfSegments();
            textProgress.GoalProgress[0] = GetNumberOfWords(CurrentSegment);
        }

        private int NumberOfSegments()
        {
            return textMetaData.Segments.Count;
        }

        private int GetNumberOfWords(int currentSegment)
        {
            return textMetaData.Segments[currentSegment].ValidWords.Count;
        }

        internal void AdvanceSegment()
        {
            CurrentSegment++;
            CurrentWord = 0;
            UpdateMax();
        }

        internal bool CanAdvanceSegment()
        {
            return textMetaData.Segments.Count > (CurrentSegment+1);
        }
    }
}
