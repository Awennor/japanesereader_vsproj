﻿using pidroh.JapaneseTextReader;
using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.persistence;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace JapaneseTextReaderConsole
{
    class JapaneseReaderAccumulatorApp
    {
        private static TextWindow mainForm;
        private static int lastSegment = -1;
        private static string phrases = "";

        internal static void Execute()
        {
            mainForm = new TextWindow();
            var thread = new Thread(() =>
            {
                Application.EnableVisualStyles();

                Application.Run(mainForm);
            });
            thread.Start();
            mainForm.TextBoxText = "test";

            var dictionaryText = File.ReadAllText("data/edict2 utf.txt");
            var storyText = File.ReadAllText("data/textdata/Gift of the Magi B.txt");
            MecabWrapper mecabWrapper = new MecabWrapper();

            JapaneseApp japp = new JapaneseApp();
            japp.Setup(mecabWrapper.Parse, dictionaryText, new LearnerData());
            japp.AddStory("bla", storyText);
            japp.ChooseStory(0);
            JapaneseReaderSuper jrn = japp.Jsr;

            //JapaneseReaderSuper jrn = new JapaneseReaderSuper(storyText, "magiwhatever", dictionaryText, mecabWrapper.Parse, new LearnerData());

            PartExerciseView view = new PartExerciseView((s) =>
            {
                mainForm.TextBoxText = s;
            });
            PartExerciseController control = new PartExerciseController(view);
            
            view.OnQuestionAnswer += (b) =>
            {
                if (b)
                {
                    Console.WriteLine("You got it right");
                }
                else
                {
                    Console.WriteLine("You got it wrong");
                }
            };
            view.OnQuestionAnswer += jrn.ExerciseResult;
            //control.ShowExercise(japp.GetReadingExercise(1));
            while (true)
            {
                int segmentProgress = jrn.GetCurrentSegment();
                if (segmentProgress > lastSegment)
                {
                    lastSegment++;
                    string phrase = jrn.GetPhrase(lastSegment);
                    while (true)
                    {
                        var words = jrn.GetWords(lastSegment);
                        ShowText(phrases);

                        mainForm.AppendBold = phrase;
                        bool enterInMenu = PickAWordMenu(jrn, words, true);
                        if (!enterInMenu)
                        {
                            break;
                        }
                    }
                    phrases += phrase;

                }
                jrn.AdvanceSegment();
                bool review = jrn.ShouldReview();
                //review = true;
                while (review)
                {
                    var ex = jrn.ReviewExercise();
                    control.ShowExercise(ex);
                    review = jrn.ShouldReview();
                }
                //ShowText(phrases);


            }
        }

        private static bool PickAWordMenu(JapaneseReaderSuper jrn, List<string> words, bool optionalShow)
        {
            bool enterInMenu = true;
            if (optionalShow)
            {
                var c = Console.ReadKey(true).KeyChar;
                enterInMenu = c == '2';
            }

            if (enterInMenu)
            {
                mainForm.AppendNormal(Environment.NewLine);
                mainForm.AppendNormal(Environment.NewLine);
                for (int i = 0; i < words.Count; i++)
                {
                    string wordNew = words[i];
                    if (!jrn.KnownWord(wordNew))
                    {

                        mainForm.AppendNormal(string.Format(
                            "[{0}] {1} /  {2} {3}"
                            , i + 1, wordNew, jrn.GetTranslation(wordNew), Environment.NewLine));
                    }
                }

                int whichWord = Console.ReadKey(true).KeyChar - '1';
                if (whichWord < 0 || whichWord > words.Count)
                {
                    return false;
                }
                var word = words[whichWord];
                jrn.LearnWord(word, jrn.GetCurrentPhrase());
            }

            return enterInMenu;
        }

        private static void ShowText(string v)
        {
            mainForm.TextBoxText = v;
        }
    }
}
