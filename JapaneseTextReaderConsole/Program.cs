﻿using JapaneseTextReader;
using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader;
using pidroh.JapaneseTextReader.analytics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace JapaneseTextReaderConsole
{
    class Program
    {



        static string[] words = new string[] {
            "Urumpt",
            "Deolom",
            "Jejurm",
            "Rutboo",
            "Abogad",
            "Torser",
            "Kricul",
            "Psycla",
            "Luritt",
            "Movant"
        };
        static string[] meanings = new string[] {
            "Water",
            "Robot",
            "King",
            "Duck",
            "Giant",
            "Bat",
            "Bench",
            "Dragon",
            "Planet",
            "Dog",
        };
        private static TextWindow mainForm;

        [STAThread]
        static void Main(string[] args)
       {

            JReadingTestApp.Execute();
            //JapaneseReaderAccumulatorApp.Execute();



            //JapaneseReaderMenuApp.JapaneseReaderMenu(dictionaryText, storyText, mecabWrapper, mainForm);

        }

        private static void ShowText(string v)
        {
            mainForm.TextBoxText = v;
        }



        private static void WordListTest()
        {
            var wordList = new WordListLearner();
            wordList.InitializeDictionary(words, meanings);
            wordList.AddWordsToLearn(words);
            PartExerciseView view = new PartExerciseView(Console.WriteLine);
            PartExerciseController partExerciseController = new PartExerciseController(view);
            LearnerWordDataAccessor learnerProfile = new LearnerWordDataAccessor();

            string wordTarget = null;


            int index = 0;
            Action<int> introduceWordMethod = (i) =>
            {
                Console.WriteLine(words[i] + " = " + meanings[i]);
            };
            Action<string> exerciseWordMethod = (s) =>
            {
                wordTarget = s;
                partExerciseController.ShowExercise(wordList.GetExercise(s));
            };
            view.OnQuestionAnswer += (b) =>
            {
                Console.WriteLine("Correct: " + b);
                //learnerProfile.LearnNewWord(wordTarget);
                learnerProfile.ExercisedWord(wordTarget, b);
                learnerProfile.Interacted();


                if (learnerProfile.ShouldReview())
                {
                    Console.WriteLine("Review");
                    exerciseWordMethod(learnerProfile.GetWordToReview());

                }
                else
                {
                    Console.WriteLine("New word");
                    introduceWordMethod(index);
                    var word = words[index];
                    index++;
                    exerciseWordMethod(word);

                }

            };
            introduceWordMethod(index);
            var w = words[index];
            index++;
            exerciseWordMethod(w);
        }
    }
}
