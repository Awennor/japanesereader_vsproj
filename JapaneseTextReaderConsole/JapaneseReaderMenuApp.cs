﻿using JapaneseTextReader.exercise;
using pidroh.JapaneseTextReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace JapaneseTextReaderConsole
{
    class JapaneseReaderMenuApp
    {
        private static TextWindow mainForm;

        public static void JapaneseReaderMenu()
        {
            mainForm = new TextWindow();
            var thread = new Thread(() =>
            {
                Application.EnableVisualStyles();

                Application.Run(mainForm);
            });
            thread.Start();
            mainForm.TextBoxText = "test";

            var dictionaryText = File.ReadAllText("data/edict2 utf.txt");
            var storyText = File.ReadAllText("data/textdata/Gift of the Magi.txt");
            MecabWrapper mecabWrapper = new MecabWrapper();

            JapaneseReaderSuper jrn = null;
           //new JapaneseReaderSuper(storyText, dictionaryText, mecabWrapper.Parse);

            PartExerciseView view = new PartExerciseView((s) =>
            {
                mainForm.TextBoxText = s;
            });
            PartExerciseController control = new PartExerciseController(view);
            view.OnQuestionAnswer += (b) =>
            {
                if (b)
                {
                    Console.WriteLine("You got it right");
                }
                else
                {
                    Console.WriteLine("You got it wrong");
                }
            };
            view.OnQuestionAnswer += jrn.ExerciseResult;

            while (true)
            {
                string currentPhrase = jrn.GetCurrentPhrase();
                mainForm.TextBoxText =
                    string.Format(
                        "{1}{0}{0}" +
                        "1)Learn new word{0}" +
                        "2)Review{0}" +
                        "3)Learn phrase {0}" +
                        "4)See phrases{0}" +
                        "5)Automated{0}", Environment.NewLine, currentPhrase);
                var k = Console.ReadKey(true).KeyChar;
                MenuAction(jrn, control, k);
            }
        }

        private static void MenuAction(JapaneseReaderSuper jrn, PartExerciseController control, char k)
        {
            string currentPhrase = jrn.GetCurrentPhrase();
            switch (k)
            {
                case '1':
                    {
                        Console.WriteLine("Learning new word");
                        string wordToLearn = jrn.GetWordForLearning();
                        string wordToLearn_Meaning = jrn.GetTranslation(wordToLearn);
                        ShowText(currentPhrase +
                            Environment.NewLine +
                            Environment.NewLine +
                            wordToLearn + " = " +
                            wordToLearn_Meaning +
                            Environment.NewLine +
                            Environment.NewLine +
                            "1) Learn word" +
                            Environment.NewLine +
                            "2) Ignore word"
                            );
                        char key = Console.ReadKey(true).KeyChar;
                        if (key == '1')
                        {
                            jrn.LearnNewWord();
                            PartChoiceExercise ex = jrn.NewWordExercise();
                            control.SetCurrentPhrase(null);
                            control.ShowExercise(ex);
                            jrn.FinishedNewWord();
                        }
                        if (key == '2')
                        {
                            jrn.IgnoreNewWord();
                        }


                        if (jrn.CanLearnNewPhrase())
                        {
                            Console.WriteLine("New phrase available");
                        }
                    }
                    break;
                case '2':
                    if (jrn.CanReview())
                    {
                        PartChoiceExercise e = jrn.ReviewExercise();
                        control.SetCurrentPhrase(null);
                        control.ShowExercise(e);
                    }
                    break;
                case '3':
                    if (jrn.CanLearnNewPhrase())
                    {

                        string phrase = jrn.LearnNewPhrase();
                        ShowText(phrase);
                        Console.ReadKey(true);
                    }
                    break;
                case '4':
                    string text = "";
                    for (int i = 0; i < jrn.PhraseLearnedAmount; i++)
                    {
                        string phrase = jrn.GetPhrase(i);
                        text += phrase + Environment.NewLine;
                    }
                    ShowText(text);
                    Console.ReadKey(true);

                    break;
                case '5':
                    char c = '1';

                    if (jrn.ShouldReview())
                    {
                        c = '2';
                    }
                    if (jrn.CanLearnNewPhrase())
                    {
                        c = '3';

                    }
                    MenuAction(jrn, control, c);
                    break;

                default:
                    break;
            }
        }

        private static void ShowText(string v)
        {
            mainForm.TextBoxText = v;
        }
    }

}
