﻿using pidroh.JapaneseTextReader;
using pidroh.JapaneseTextReader.analytics;
using pidroh.JapaneseTextReader.persistence;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace JapaneseTextReaderConsole
{
    class JReadingTestApp
    {
        private static TextWindow mainForm;
        private static int lastSegment = -1;
        private static string phrases = "";

        internal static void Execute()
        {
            mainForm = new TextWindow();
            var thread = new Thread(() =>
            {
                Application.EnableVisualStyles();

                Application.Run(mainForm);
            });
            thread.Start();
            mainForm.TextBoxText = "test";

            var dictionaryText = File.ReadAllText("data/edict2 utf.txt");
            var storyText = File.ReadAllText("data/textdata/Gift of the Magi B.txt");
            MecabWrapper mecabWrapper = new MecabWrapper();

            JapaneseApp japp = new JapaneseApp();
            japp.Setup(mecabWrapper.Parse, dictionaryText, new LearnerData());
            japp.AddStory("bla", storyText);
            japp.ChooseStory(0);
            JapaneseReaderSuper jrn = japp.Jsr;

            //JapaneseReaderSuper jrn = new JapaneseReaderSuper(storyText, "magiwhatever", dictionaryText, mecabWrapper.Parse, new LearnerData());

            PartExerciseView view = new PartExerciseView((s) =>
            {
                mainForm.TextBoxText = s;
            });
            PartExerciseController control = new PartExerciseController(view);
            Action<bool> ExerciseResultMethod = null;
            ExerciseResultMethod = japp.ReadingExerciseManager.ReportOnExercise;
            view.OnQuestionAnswer += (b) =>
            {
                if (b)
                {
                    Console.WriteLine("You got it right");
                }
                else
                {
                    Console.WriteLine("You got it wrong");
                }
                ExerciseResultMethod(b);
            };
            //view.OnQuestionAnswer += jrn.ExerciseResult;

            while (true)
            {
                int segmentProgress = jrn.GetCurrentSegment();
                if (segmentProgress > lastSegment)
                {
                    lastSegment++;
                    string phrase = jrn.GetPhrase(lastSegment);
                    var words = jrn.GetWords(lastSegment);
                    ShowText(phrases);

                    mainForm.AppendBold = phrase;
                    //bool enterInMenu = PickAWordMenu(jrn, words, true);
                    var ex = japp.GetNewReadingExercise();
                    while (ex != null)
                    {
                        control.ShowExercise(ex);
                        ex = japp.GetNewReadingExercise();
                    }
                    


                    //control.ShowExercise(ex);

                    phrases += phrase;

                }
                jrn.AdvanceSegment();
                control.ShowExercise(japp.GetReadingReviewExercise());

                //bool review = jrn.ShouldReview();
                //review = true;
                //while (review)
                //{
                //    var ex = jrn.ReviewExercise();
                //    control.ShowExercise(ex);
                //}



            }
        }


        private static void ShowText(string v)
        {
            mainForm.TextBoxText = v;
        }
    }
}
