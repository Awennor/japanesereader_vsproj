﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JapaneseTextReaderConsole
{
    public partial class TextWindow : Form
    {

        public string TextBoxText
        {
            set
            {
                
                textBox1.Text = value;
                //textBox1.SelectAll();
                //textBox1.SelectionBackColor = Color.Transparent;
                //textBox1.Rtf = @"{\rtf1\ansi this word is \b bold \b0 }";
            }
        }

        public string AppendBold
        {
            set
            {
                textBox1.SelectAll();
                textBox1.SelectionBackColor = Color.Transparent;
                textBox1.Select(textBox1.TextLength, 0);
                textBox1.SelectionBackColor = Color.Yellow;
                textBox1.AppendText(value);
                //textBox1.Rtf = @"{\rtf1\ansi this word is \b bold \b0 }";
            }
        }

        public TextWindow()
        {
            InitializeComponent();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        internal void AppendNormal(string v)
        {
            
            textBox1.Select(textBox1.TextLength, 0);
            textBox1.SelectionBackColor = Color.Transparent;
            textBox1.AppendText(v);
        }
    }
}
