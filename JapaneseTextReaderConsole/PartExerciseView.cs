﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JapaneseTextReaderConsole
{

    class PartExerciseView
    {
        public string Prompt { set; get; }
        public List<string> DummyTexts { set; get; } = new List<string>();
        public string Answer { set; get; }
        public int SelectedPortion { set; get; }
        public int NumberOfPortions { set; get; }

        public event Action<bool> OnQuestionAnswer;
        public Action<string> ShowTextMethod { set; get; }
        public string CurrentPhrase { get; internal set; }

        List<string> aux = new List<string>();

        public PartExerciseView(Action<string> showTextMethod)
        {
            ShowTextMethod = showTextMethod;
        }

        internal void Show()
        {
            aux.Clear();
            string text = "";
            if (CurrentPhrase != null)
            {
                text = CurrentPhrase + Environment.NewLine + Environment.NewLine;
            }
            text += Prompt + "\r\n" + SelectedPortion + "\r\n\r\n";
            for (int i = 0; i < DummyTexts.Count; i++)
            {
                aux.Add(DummyTexts[i]);
            }
            aux.Add(Answer);
            aux.Shuffle();
            for (int i2 = 0; i2 < aux.Count; i2++)
            {
                text += (i2 + 1) + ") " + aux[i2] + "\r\n";

            }
            int answer = aux.IndexOf(Answer);
            ShowTextMethod(text);
            //MessageBox.Show(text);
            //Console.WriteLine(text);
            int input = Console.ReadKey(true).KeyChar - '0' - 1;
            bool correct = answer == input;
            OnQuestionAnswer(correct);

        }
    }


}
