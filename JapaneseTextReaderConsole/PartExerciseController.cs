﻿using JapaneseTextReader.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JapaneseTextReaderConsole
{
    class PartExerciseController
    {
        PartExerciseView view;
        //public event Action<bool> OnQuestionAnswer;

        public PartExerciseController(PartExerciseView view)
        {
            this.view = view;
            
        }

        

        public void ShowExercise(PartChoiceExercise exercise) {

            view.Prompt = exercise.ContextPhrase+'\n'+ exercise.Prompt ;
            view.NumberOfPortions = exercise.AnswerParts.Count;
            int part = 0;
            view.SelectedPortion = part;
            view.DummyTexts.Clear();
            foreach (var item in exercise.Dummies[part])
            {
                view.DummyTexts.Add(item);
            }
            view.Answer = exercise.AnswerParts[part];
            view.Show();
        }

        internal void SetCurrentPhrase(string currentPhrase)
        {
            view.CurrentPhrase = currentPhrase;
        }
    }
}
